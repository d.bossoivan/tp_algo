def championnat(n):
    n_prime = n if n % 2 == 0 else n + 1
    match_days = []

    for j in range(1, n_prime):
        match_day = []
        for i in range(1, n_prime // 2 + 1):
            home_team = n_prime if i == 1 else ((j + i - 2) % (n_prime - 1)) + 1
            away_team = ((j - i + n_prime - 1) % (n_prime - 1)) + 1
            match_day.append((home_team, away_team))
        match_days.append(match_day)

    return match_days

def afficher_journee(match_day, day_number):
    print(f"Journée N°{day_number}:")
    for match in match_day:
        if match[0] != n and match[1] != n:
            print(f"équipe {match[0]} reçoit équipe {match[1]}")
        else:
            print(f"équipe {match[0]} est au repos")

if __name__ == '__main__':

    n = 6  # Exemple avec 6 équipes
    championnat_6_equipes = championnat(n)


    for i, match_day in enumerate(championnat_6_equipes, start=1):
        afficher_journee(match_day, i)