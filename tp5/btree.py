from __future__ import annotations

from collections import deque
from dataclasses import dataclass


@dataclass
class Node:
    data: int
    left: Node | None = None
    right: Node | None = None


@dataclass
class BinaryTree:
    root: Node = None


def _build_node(k, nodes) -> Node | None:
    if k >= len(nodes) or nodes[k] is None:
        return None
    nn = n_new(nodes[k])
    left = _build_node(2 * k + 1, nodes)
    right = _build_node(2 * k + 2, nodes)
    nn.left = left
    nn.right = right
    return nn


def bt_new(nodes: list[int | None] | None = None) -> BinaryTree:
    nodes = nodes or []
    nn = _build_node(0, nodes)
    bt = BinaryTree(nn)
    return bt


def bt_is_empty(bt: BinaryTree) -> bool:
    if bt.root is None:
        return True
    else:
        return False


def bt_root(bt: BinaryTree) -> Node:
    if bt_is_empty(bt):
        raise IndexError("Arbre vide")
    else:
        return bt.root


def n_new(k: int, left: Node | None = None, right: Node | None = None) -> Node:
    new_node = Node(k, left, right)
    return new_node


def n_get(n: Node) -> int:
    return n.data


def n_set(n: Node, k: int) -> None:
    n.data = k


def n_left(n: Node) -> Node | None:
    return n.left


def n_right(n: Node) -> Node | None:
    return n.right


def n_is_leaf(n: Node) -> bool:
    if n.left is n.right is None:
        return True
    else:
        return False


def bt_height(bt: BinaryTree) -> int:
    def _root_height(root: Node | None) -> int:
        if root is None:
            return -1
        else:
            return 1 + max(_root_height(root.left), _root_height(root.right))

    return _root_height(bt.root)


def bt_size(bt: BinaryTree) -> int:
    def _node_weight(root: Node | None) -> int:
        if root is None:
            return 0
        else:
            return 1 + _node_weight(root.left) + _node_weight(root.right)

    return _node_weight(bt.root)


def bt_str(bt: BinaryTree) -> str:
    if bt_is_empty(bt):
        return "Error"
    else:
        res = ""
        f = deque()
        f.append((bt.root, 0))
        c_lvl: int = 0
        while len(f) != 0:
            c, lvl = f.popleft()
            if c is not None:
                if lvl == c_lvl:
                    res = f'{res} ({c.data})'
                else:
                    res = f'{res}\n ({c.data})'
                    c_lvl = lvl
                if bt.root.left is not None:
                    f.append((c.left, lvl + 1))
                if bt.root.right is not None:
                    f.append((c.right, lvl + 1))
            #else:
               # res += "None"
        return res


if __name__ == '__main__':
    tree = bt_new([2, 1, 4, 0, None, 3, 5])
    print(bt_str(tree))
