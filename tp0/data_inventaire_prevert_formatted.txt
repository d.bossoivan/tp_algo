0. Une pierre
1. deux maisons
2. trois ruines
3. quatres fossoyeurs
4. un jardin
5. des fleurs
6. 
7. un raton laveur
8. 
9. une douzaine d'huîtres un citron un pain
10. un rayon de soleil
11. une lame de fond
12. six musiciens
13. une porte avec son paillasson
14. un monsieur décoré de la légion d'honneur
15. 
16. un autre raton laveur
17. 
18. un sculpteur qui sculpte des napoléon
19. la fleur qu'on appelle souci
20. deux amoureux sur un grand lit
21. un receveur des contributions une chaise trois dindons
22. un ecclésiastique un furoncle
23. une guêpe
24. un rein flottant
25. une écurie de courses
26. un fils indigne deux frères dominicains trois sauterelles
27. un strapontin
28. deux filles de joie un oncle cyprien
29. une Mater dolorosa trois papas gâteau deux chèvres de
30. Monsieur Seguin
31. un talon Louis XV
32. un fauteuil Louis XVI
33. un buffet Henri II deux buffets Henri III trois buffets
34. Henri IV
35. un tiroir dépareillé
36. une pelote de ficelle deux épingles de sûreté un monsieur
37. âgé
38. une Victoire de samothrace un comptable deux aides-
39. comptables un homme du monde deux chirurgiens
40. trois végétariens
41. un cannibale
42. une expédition coloniale un cheval entier une demi-
43. pinte de bon sang une mouche tsé-tsé
44. un homard à l'américaine un jardin à la française
45. deux pommes à l'anglaise
46. un face-à-main un valet de pied un orphelin un poumon
47. d'acier
48. un jour de gloire
49. une semaine de bonté
50. un mois de marie
51. une année terrible
52. une minute de silence
53. une seconde d'inattention
54. et...
55. 
56. cinq ou six ratons laveurs
57. 
58. un petit garçon qui entre à l'école en pleurant
59. un petit garçon qui sort de l'école en riant
60. une fourmi
61. deux pierres à briquet
62. dix-sept éléphants un juge d'instruction en vacances
63. assis sur un pliant
64. un paysage avec beaucoup d'herbe verte dedans
65. une vache
66. un taureau
67. deux belles amours trois grandes orgues un veau
68. marengo
69. un soleil d'austerlitz
70. un siphon d'eau de Seltz
71. un vin blanc citron
72. un Petit Poucet un grand pardon un calvaire de pierre
73. une échelle de corde
74. deux sœoeurs latines trois dimensions douze apôtres mille
75. et une nuits trente-deux positions six parties du
76. monde cinq points cardinaux dix ans de bons et
77. loyaux services sept péchés capitaux deux doigts de
78. la main dix gouttes avant chaque repas trente jours
79. de prison dont quinze de cellule cinq minutes
80. d'entracte
81. 
82. et...
83. 
84. plusieurs ratons laveurs.
85. 
86. (Jacques Prévert, Paroles, 1946)
