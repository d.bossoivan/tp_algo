from ctypes import Array, c_int


def saisir_entier() -> int :
     v = input("Entrez l'entier :")
     return v

def alloc(m: int) -> Array:
     IntArrayType = c_int * m # création d'un type "tableau de m entiers"
     return IntArrayType()
#print('vous avez saisi: '(saisir_entier()))

if __name__ == '__main__':
     print(saisir_entier())