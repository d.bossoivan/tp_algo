
if __name__ == '__main__':

    with open('data_inventaire_prevert.txt', 'r') as file:  # ouverture du fichier (mode r: read)
        poem = file.read()  # lecture du contenu du fichier

    # Replace ';' by '\n'
    poem = poem.replace(';', '\n')

    # Diviser le texte en lignes, renvoie un tableau ou chaque case contient une ligne
    lines = poem.split('\n')

    # Ajouter les numéros au début de chaque ligne, enumerate renvoie ('0 a','0 b' , etc')
    numbered_poem = ''
    for i, line in enumerate(lines):
        numbered_poem += f"{i}. {line}\n"

    # Écrire le poème formaté dans un nouveau fichier
    with open('data_inventaire_prevert_formatted.txt', 'w') as file:
        file.write(numbered_poem)  # écriture des données dans le fichier
