from __future__ import annotations
from dataclasses import dataclass
from typing import Iterator


@dataclass
class Cell:
    next: Cell | None = None
    previous: Cell = None
    data: int | None = None


@dataclass
class LinkedList:
    length: int
    sen: Cell = None


def ll_new(initial_l: list[int] | None = None) -> LinkedList:
    sen = Cell()
    sen.next = sen
    sen.previous = sen
    if initial_l is None or len(initial_l) == 0:
        ll = LinkedList(0, sen)
    else:
        ll = LinkedList(0, sen)
        for i in range(len(initial_l)):
            ll_append(ll, initial_l[i])
    return ll


def ll_is_empty(ll: LinkedList) -> bool:
    if ll.sen.next is ll.sen:
        return True
    else:
        return False


def ll_len(ll: LinkedList) -> int:
    length = ll.length
    return length


def ll_head(ll: LinkedList) -> Cell:
    if ll_is_empty(ll):
        raise IndexError("Liste vide")
    else:
        head = ll.sen.next
    return head


def ll_tail(ll: LinkedList) -> Cell:
    if ll_is_empty(ll):
        raise IndexError("Liste vide")
    else:
        head = ll.sen.previous
    return head


def ll_get(l: LinkedList, idx: Cell) -> int:
    if ll_is_empty(l):
        raise IndexError("liste vide")
    else:
        val = idx.data
    return val


def ll_set(ll: LinkedList, idx: Cell, item: int) -> LinkedList:
    if ll_is_empty(ll):
        raise IndexError("liste vide")
    else:
        idx.data = item
        return ll


def ll_iter_cells(ll: LinkedList) -> Iterator[Cell]:
    if not ll_is_empty(ll):
        current: Cell = ll_head(ll)
        for i in range(ll_len(ll)):
            yield current
            current = current.next


def ll_reversed_iter_cells(ll: LinkedList) -> Iterator[Cell]:
    current: Cell = ll.sen.previous
    for i in range(ll_len(ll)):
        yield current
        current = current.previous


def ll_insert(ll: LinkedList, item: int, neighbor: Cell | None = None, after: bool = True) -> LinkedList:
    neighbor = neighbor or ll.sen
    if after:
        cell_new = Cell(neighbor.next, neighbor, item)
        neighbor.next = cell_new
        cell_new.next.previous = cell_new
    else:
        cell_new = Cell(neighbor, neighbor.previous, item)
        neighbor.previous = cell_new
        cell_new.previous.next = cell_new
    ll.length += 1
    return ll


def ll_append(l: LinkedList, item: int) -> LinkedList:
    ll_insert(l, item, None, False)
    return l


def ll_prepend(ll: LinkedList, item: int) -> LinkedList:
    ll_insert(ll, item, None, True)
    return ll


def ll_str(ll: LinkedList) -> str:
    if ll_is_empty(ll):
        return "[]"
    s: str = "["
    for c in ll_iter_cells(ll):
        s += str(ll_get(ll, c)) + ", "

    s = s[:-2] + "]"
    return s


def ll_lookup(ll: LinkedList, item: int) -> Cell | None:
    for c in ll_iter_cells(ll):
        if c.data == item:
            return c
    return None


def ll_cell_at(ll: LinkedList, i: int) -> Cell:
    if i > ll_len(ll) or i < 0 or ll_is_empty(ll):
        raise IndexError('Wrong index')
    else:
        cpt = 0
        for c in ll_iter_cells(ll):
            if cpt == i:
                return c
            else:
                cpt += 1


def ll_remove(ll: LinkedList, cell: Cell) -> LinkedList:
    if ll_is_empty(ll):
        raise IndexError("Liste vide")
    else :
        for i in range (ll_len(ll)) :
            current = ll_lookup(ll, i)
            if current is cell :
                current.previous.next = current.next
                current.next.previous = current.previous
                return ll
            else:
                raise IndexError("given cell not found in Linked list")


def ll_extend(l1: LinkedList, l2: LinkedList) -> LinkedList:
    raise NotImplementedError("LinkedList ll_extend function not implemented yet")
