def pgcd(a, b):
    if b == 0:
        return a
    else:
        return pgcd(b, a % b)


print("Le pgcd est", pgcd(18, 6))
