import math
from dataclasses import dataclass
from ctypes import Array, c_int

from tp0.util import alloc


def encode(entry: str) -> int:
    t = ""
    res = 0
    for i in range(len(entry)):
        t += str(ord(entry[i]))
        for j in range(len(t)):
            res = res + (int(t[j]) * j + 1)
    return res


def compress(entry: int, m: int = 1_000) -> int:
    return entry % m


def harsher(key: str, m: int = 1_000) -> int:
    return compress(encode(key), m)


@dataclass
class Item:
    k: str
    v: int


@dataclass
class HashMap:
    size: int
    hash_table: list[Item | None]


def hm_size(aa: HashMap) -> int:
    return aa.size


def hm_is_empty(aa: HashMap) -> bool:
    return aa.size == 0


def hm_new(m: int = 1 << 12) -> HashMap:
    tab = [None] * m
    hm = HashMap(0, tab)
    return hm


def h(k: str, i: int, mod: int) -> int:
    hashcode = harsher(k, mod) #hashcode correspondant à la clé K
    return (hashcode + int(i**2 / 2 + i / 2)) % mod


def hm_get(aa: HashMap, k: str) -> int:
    m = len(aa.hash_table)
    i: int = 0
    idx = h(k, i, m)  #index du hash_table
    #tant que la case à l'index idx est None ou la clé a cette case est differente de l'entrée k;
    #?Elle peut etre None mais la valeur se trouve plus loin?, on avance avec j, seulement j a un pas de +1 ok?
    while aa.hash_table[idx] is None or aa.hash_table[idx].k != k:
        i += 1
        if i >= m:
            exit(1)     # element pas dans l'ensemble
        idx = h(k, i, m)
    it = aa.hash_table[idx]
    return it.v


def hm_put(aa:HashMap, k: str, v: int) -> HashMap:
    m = len(aa.hash_table)
    i: int = 0
    if m == aa.size:            #if hashtabe is already  full
       raise OverflowError
    idx = h(k, i, m)  #index du hash_table
    while aa.hash_table[idx] is not None:
        i += 1
        idx = h(k, i, m)
    aa.hash_table[idx] = Item(k, v)
    aa.size += 1
    return aa


def hm_delete(aa: HashMap, k: str) -> HashMap:
    m = len(aa.hash_table)
    i: int = 0
    idx = h(k, i, m)
    while aa.hash_table[idx] is not None:

        if k == aa.hash_table[idx].k:
            aa.hash_table[idx] = None
        i += 1
        if i >= m:
            exit(1)  # element pas dans l'ensemble
        idx = h(k, i, m)

    aa.size -= 1
    return aa


if __name__ == '__main__':
    with open("liste_de_mots_frgut.txt", "r") as lm: # ouverture du fichier (mode r: read)
        histogram_hash = {}
        histogram_encode = {}
        for mot in lm:
            h = hash(mot)
            enc = encode(mot)
            if h in histogram_hash:
                histogram_hash[h] += 1
            else:
                histogram_hash[h] = 1
            if enc in histogram_encode:
                histogram_encode[enc] += 1
            else:
                histogram_encode[enc] = 1

    print('hash')
    for k, v in histogram_hash.items():
        if v > 1:
            print(k, v)
    print('encode')
    s = c = 0
    for k, v in histogram_encode.items():
        if v > 1:
            c = c + v
            s += 1
    print(f'nb de colision = {s}, moy = {c / s} maximum = {max(histogram_encode.values())}')
