from tp3.ex4 import ArrayList, al_len, al_new, al_str


def al_get(tab: ArrayList, i: int) -> int:
    if i > al_len(tab):
        raise IndexError("Position incorrecte")
    else:
        c = tab.content[i]
        return c


def al_insert(tab: ArrayList, i: int, item: int) -> ArrayList:
    tab.current_size += 1
    tab_initial = tab.content
    for j in range(al_len(tab) - 1, i, -1):
        tab_initial[j] = tab_initial[j - 1]
    tab_initial[i] = item

    return tab


al = al_new(5, [10, 20, 30])
al2 = al_insert(al, 1, 45)
val = al_get(al2, 2)
print('la valeur est : ', val)
print('le tableau final est', al_str(al2))
