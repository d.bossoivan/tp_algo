from dataclasses import dataclass
from ctypes import Array, c_int

from tp0.util import alloc


@dataclass
class ArrayList :
    max_size : int
    tab: Array
    current_size: int

def al_new(m : int = 10, l : list[int] = None) -> ArrayList:
    assert not l or m >= len(l)  #len(None)
    if l is None:
        l =[]
    t = alloc(m)
    for i in range(len(l)):
        t[i] = l[i]
    s=len(l)
    aL = ArrayList(m, t, s)
    return aL

def al_len(tab: ArrayList) -> int:
    size = tab.current_size
    return size

def al_is_empty(tab:ArrayList) -> bool :
    content = tab.tab or []
    if content == [] :
        return True
    else :
        return False