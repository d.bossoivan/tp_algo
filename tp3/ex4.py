from ctypes import Array, c_int
from dataclasses import dataclass


def alloc(m: int) -> Array:
    IntArrayType = c_int * m  # création d'un type "tableau de m entiers"
    return IntArrayType()  # déclaration et initialisation à zéro du tableau


@dataclass
class ArrayList:
    max_size: int
    content: Array
    current_size: int


def al_new(m: int, l: list[int]) -> ArrayList:
    tab = alloc(m)
    assert m > len(l)
    for i in range(len(l)):
        tab[i] = l[i]
    al = ArrayList(m, tab, len(l))
    return al


def al_len(tab: ArrayList) -> int:
    c_size = tab.current_size
    return c_size


def al_is_empty(tab: ArrayList) -> bool:
    c = tab.current_size
    if c == 0:
        return True
    else:
        return False


def al_str(tab: ArrayList) -> str:
    ch = '['
    for i in range(al_len(tab)):
        if i== al_len(tab):
            ch += str(tab.content[i])
        else:
            ch += str(tab.content[i]) + ','
    ch += ']'
    return ch


#al = al_new(5, [1, 2, 3])
#ch = al_str(al)
#print('le tab:', ch)